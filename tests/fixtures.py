"""Set up global fixtures for pytest."""
import os
import urllib.parse
from unittest import mock

import flask
import pytest
import requests
import yaml
from minio import Minio

import encrypting_s3_proxy

__all__ = []


def export(f):
    __all__.append(f.__name__)
    return f


def is_responsive(url, require_code: list[int] = None):
    try:
        response = requests.get(url)
        if require_code:
            return response.status_code in require_code
        return True
    except ConnectionError:
        return False


@export
@pytest.fixture(scope="session")
def docker_compose_command() -> str:
    """Override pytest-docker to use podman-compose."""
    return "podman-compose"


@export
@pytest.fixture(scope="session")
def minio(docker_ip, docker_services) -> str:
    url = f"http://{docker_ip}:9000"
    docker_services.wait_until_responsive(
        timeout=10.0, pause=0.1, check=lambda: is_responsive(url)
    )
    return url


@export
@pytest.fixture(scope="session")
def app(minio) -> flask.Flask:
    with mock.patch.dict(os.environ, {"FLASK_S3_BACKEND": minio}):
        return encrypting_s3_proxy.init_app()


@export
@pytest.fixture(scope="session")
def docker_compose_file_content(docker_compose_file):
    with open(docker_compose_file, "r") as f:
        loaded = yaml.safe_load(f)
    return loaded


@export
@pytest.fixture(scope="session")
def minio_credentials(docker_compose_file_content):
    env = docker_compose_file_content["services"]["minio"]["environment"]
    return env["MINIO_ROOT_USER"], env["MINIO_ROOT_PASSWORD"]


@export
@pytest.fixture(scope="module")
def s3_backend(minio, minio_credentials):
    parsed = urllib.parse.urlparse(minio)
    return Minio(
        endpoint=parsed.netloc,
        secure=(parsed.scheme == "https"),
        access_key=minio_credentials[0],
        secret_key=minio_credentials[1],
    )


@export
@pytest.fixture(scope="module")
def s3_frontend(live_server, minio_credentials):
    url = f"http://{live_server.host}:{live_server.port}"
    parsed = urllib.parse.urlparse(url)
    return Minio(
        endpoint=parsed.netloc,
        secure=(parsed.scheme == "https"),
        access_key=minio_credentials[0],
        secret_key=minio_credentials[1],
    )


@export
@pytest.fixture(scope="class")
def bucket_name(request, s3_backend) -> str:
    """Generate a valid bucket name for a test.

    :return: bucket name
    """
    return request.node.name.lower().translate(
        str.maketrans(
            {
                "_": "-",
                "/": "-",
                "[": ".",
                "]": None,  # remove
            }
        )
    )


@export
@pytest.fixture(scope="class")
def backend_bucket(request, bucket_name, s3_backend) -> str:
    """Generate a bucket name and create it on the S3 backend.

    :return: bucket name
    """
    s3_backend.make_bucket(bucket_name)
    print(f"Created backend bucket {bucket_name}")
    yield bucket_name
    s3_backend.remove_objects(
        bucket_name, s3_backend.list_objects(bucket_name, recursive=True)
    )
    s3_backend.remove_bucket(bucket_name)
