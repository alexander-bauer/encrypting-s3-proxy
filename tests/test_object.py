import io

import pytest

DATA = {
    "top-level-key": b"This is a cleartext sentence for top-level-key.",
    "multiline-data": b"Line one\nline two",
    "binary-data": b"\x00\xFF\x00\xFF",
    "something/nested": b"Nested data, nothing sepecial.",
    "something/v/e/r/y/deeply/nested": b"This data is very deeply nested.",
}


@pytest.fixture(params=DATA.keys())
def data(request) -> tuple[str, bytes]:
    return request.param, DATA[request.param]


@pytest.fixture
def put_object(s3_frontend, backend_bucket, data) -> tuple[str, bytes]:
    """Put an object into a new bucket through the proxy.

    Tears down automatically.

    :yield: a tuple containing the bucket, the path to the object, and the original
            data.
    """
    key, value = data

    # Write the data through the proxy.
    s3_frontend.put_object(backend_bucket, key, io.BytesIO(value), len(value))
    yield backend_bucket, key, value
    s3_frontend.delete_object(backend_bucket, key)


@pytest.mark.xfail()
class TestObjectContentEncryption:
    @pytest.mark.dependency(name="write")
    def test_encrypted_write(self, s3_frontend, s3_backend, put_object):
        bucket, key, value = put_object

        # Read the data through the backend, and ensure that it is encrypted.
        response = s3_backend.get_object(bucket=bucket, object_name=key)
        backend_data = response.read()

        # Check that they are not equal.
        assert value != backend_data
        # TODO: check for equivalence to expected encrypted value.

    @pytest.mark.dependency(name="read")
    def test_encrypted_read(self, s3_frontend, s3_backend, backend_bucket, put_object):
        bucket, key, value = put_object

        # Read the data through the proxy.
        response = s3_frontend.get_object(bucket=bucket, object_name=key)
        frontend_data = response.read()

        # Check that they are not equal.
        assert value == frontend_data
