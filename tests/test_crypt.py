import io

import pytest

from encrypting_s3_proxy.crypt import CryptEngine, PasswordEngine


@pytest.mark.parametrize(
    "password, cleartext, ciphertext",
    [
        ("my-first-password", b"Some sample data.", b"Some output bytes"),
    ],
)
class TestFernetEngine:
    @pytest.fixture
    def engine(self, password) -> FernetEngine:
        return FernetEngine(password=password)

    def test_encrypt(self, engine, cleartext, ciphertext):
        with engine.encrypt(io.BytesIO(cleartext)) as c:
            actual_ciphertext = c.read()
            assert actual_ciphertext == ciphertext

    def test_decrypt(self, engine, cleartext, ciphertext):
        with engine.decrypt(io.BytesIO(ciphertext)) as c:
            actual_cleartext = c.read()
            assert actual_cleartext == cleartext
