import urllib

import pytest

from encrypting_s3_proxy.adapter import (
    Adapter,
    AdapterCapabilities,
    ObjectContentEncryptionAdapter,
)
from encrypting_s3_proxy.crypt import PasswordEngine


def test_abc_not_instantiatable():
    cls = Adapter
    with pytest.raises(TypeError) as excinfo:
        cls()

    assert "abstract class" in str(excinfo)


@pytest.fixture
def object_content_encryption_adapter(minio, minio_credentials):
    parsed = urllib.parse.urlparse(minio)
    engine = PasswordEngine(password="pytest")
    return ObjectContentEncryptionAdapter(
        endpoint=parsed.netloc,
        secure=(parsed.scheme == "https"),
        access_key=minio_credentials[0],
        secret_key=minio_credentials[1],
    )
