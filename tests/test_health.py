from flask.testing import FlaskClient


def test_health(client: FlaskClient) -> None:
    r = client.get("/health")
    assert r.status_code == 200
    assert r.json == {"healthy": True}


def test_ready(client: FlaskClient) -> None:
    r = client.get("/ready")
    assert r.status_code == 200
    assert r.json == {"ready": True}
