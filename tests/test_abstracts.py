import pytest

from encrypting_s3_proxy.adapter import Adapter
from encrypting_s3_proxy.crypt import CryptEngine

_ABCs = [Adapter, CryptEngine]


@pytest.mark.parametrize("cls", _ABCs, ids=[cls.__name__ for cls in _ABCs])
def test_abc_not_instantiatable(cls):
    with pytest.raises(TypeError) as excinfo:
        cls()

    assert "abstract class" in str(excinfo)
