import pytest


class TestBucketManagement:
    @pytest.mark.dependency(name="create")
    def test_create(self, request, bucket_name, s3_frontend):
        """Create a bucket through the front end and ensure it exists."""
        s3_frontend.make_bucket(bucket_name)
        assert s3_frontend.bucket_exists(bucket_name)

    @pytest.mark.dependency(name="list", depends=["create"])
    def test_list(self, request, bucket_name, s3_frontend):
        """List buckets and ensure we can see the one created earlier."""
        if not s3_frontend.bucket_exists(bucket_name):
            pytest.error(f"Precondition failed: expected {bucket_name} to exist")
        buckets = s3_frontend.list_buckets()
        assert bucket_name in {b.name for b in buckets}

    @pytest.mark.dependency(name="delete", depends=["list"])
    def test_delete(self, request, bucket_name, s3_frontend):
        """Delete the bucket we successfully listed."""
        s3_frontend.remove_bucket(bucket_name)
        assert not s3_frontend.bucket_exists(bucket_name)
