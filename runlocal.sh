#!/bin/bash

COMPOSE_FILE="$(dirname $0)/tests/docker-compose.yml"
COMPOSE="podman-compose -f $COMPOSE_FILE"

trap '$COMPOSE down --volumes' EXIT;

set -eux

$COMPOSE up -d
FLASK_S3_BACKEND=http://localhost:9000 gunicorn --reload --log-level DEBUG 'encrypting_s3_proxy:init_app()'
