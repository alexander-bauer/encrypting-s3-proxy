"""CryptEngine implementations using locally-provided passwords."""
import contextlib
import io
import os

from cryptography.fernet import Fernet
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.kdf.pbkdf2 import PBKDF2HMAC

from .engine import CryptEngine

DEFAULT_HASH_FUNCTION = hashes.SHA512()
DEFAULT_KEY_LENGTH = 32
DEFAULT_ITERATIONS = 210000  # OWASP 2023 recommendation for SHA512


class FernetEngine(CryptEngine):
    """Transparently encrypt/decrypt S3 object data using a password.

    A password is supplied to the constructor. It is turned into
    cryptographically-suitable keys using the `PBKDF2 key derivation function
    <https://cryptography.io/en/latest/hazmat/primitives/key-derivation-functions/#pbkdf2>`_.
    using the provided ``object_id`` as a salt. Then, the resulting ``key`` is provided
    to the `Fernet symmetric encryption recipe
    <https://cryptography.io/en/latest/fernet/>`_.

    .. warning::

        The key derivation function uses a salt derived from the ``object_id``. This means
        that moving an object requires complete re-encoding.
    """

    def __init__(
        self,
        password: str,
        hash_algorithm: hashes.Hash = DEFAULT_HASH_FUNCTION,
        key_length: int = DEFAULT_KEY_LENGTH,
        iterations: int = DEFAULT_ITERATIONS,
    ) -> None:
        self.password = password
        self.hash_algorithm = hash_algorithm
        self.key_length = key_length
        self.iterations = iterations

    def _salt_from_object_id(self, object_id: str) -> bytes:
        """Hash an ``object_id`` to generate a suitable password salt."""
        digest = hashes.Hash(self.hash_algorithm())
        digest.update(object_id.encode("utf-8"))
        return digest.finalize()

    def _kdf_from_object_id(self, object_id:, str) -> PBKDF2HMAC:
        """Instantiate a KDF for a particular ``object_id``.

        This combines the stored object with a salt generated for a particular
        ``object_id`` (see ``_salt_from_object_id()``), and supplies these to the PBKDF2
        key stretching algorithm.
        """
        salt = self._salt_from_object_id(object_id)
        kdf = PBKDF2HMAC(self.hash_algorithm(), length=self.key_length, salt=salt,
                iterations=self.iterations)
        return kdf

    def _key_from_object_id(self, object_id: str) -> bytes:
        """Use the KDF to generate a Fernet-suitable key for an ``object_id``."""
        return self._kdf_from_object_id(object_id).derive(self.password)

    def _fernet_from_object_id(self, object_id: str) -> Fernet:
        """Prepare a ``Fernet`` instance suitable for encryption or decryption for a
        particular object_id.
        """
        return Fernet(self._key_from_object_id(object_id))

    def encrypt(self, object_id: str, object_data: io.IOBase) -> bytes:
        """Encrypt a chunk of data."""
        return self._fernet_from_object_id(object_id).encrypt(object_data.read())

    def decrypt(self, input: io.IOBase, key_id: str = None) -> bytes:
        """Decrypt a chunk of data."""
        return self._fernet_from_object_id(object_id).decrypt(object_data.read())
