"""Abstract CryptEngine definition for concrete subclassing."""
import abc
import io

import minio


class CryptEngine(abc.ABC):
    """Base class for transparent encrypt/decrypt operations.

    Cryptography operations must work on variable-length chunks of data. Returned chunks
    are not required to be the same length as the inputs.
    """

    @abc.abstractmethod
    def encrypt(self, object_id: str, object_data: io.IOBase) -> bytes:
        """Encrypt a chunk of data."""

    @abc.abstractmethod
    def decrypt(self, object_id: str, object_data: io.IOBase) -> bytes:
        """Decrypt a chunk of data."""
