"""Tools for transparently modifying S3 operations.

S3 operations are performed using the ``minio`` library. Adapters are subclasses of
``minio.Client``.

Typical operations with
"""
