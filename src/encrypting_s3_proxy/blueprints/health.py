"""Blueprints for health and readiness logic."""
from flask import Blueprint, Response, jsonify

blueprint = Blueprint(__name__.split(".")[-1], __name__)


@blueprint.route("/health")
def health() -> Response:
    """Return health information as JSON."""
    return jsonify({"healthy": True})


@blueprint.route("/ready")
def ready() -> Response:
    """Return readiness information as JSON."""
    return jsonify({"ready": True})
