"""Blueprint for application access logging."""
from flask import Blueprint, Response, current_app, request

blueprint = Blueprint(__name__.split(".")[-1], __name__)


@blueprint.after_app_request
def access_log(response: Response) -> Response:
    """Log every access request."""
    current_app.logger.info(
        "%s %s %s %s %s",
        request.remote_addr,
        request.scheme,
        request.method,
        request.full_path,
        response.status_code,
    )
    return response
