"""Collection of all blueprints for the encrypting S3 proxy."""
import importlib
import os
import warnings

from flask import Blueprint

__all__ = ["blueprints"]

# Import all blueprints for the app.
blueprints: list[Blueprint] = []
for module_file in os.listdir(os.path.dirname(__file__)):
    if module_file.endswith(".py") and not module_file.startswith("_"):
        module_name = module_file[:-3]
        # Relative import using this package as the base.
        module = importlib.import_module(f".{module_name}", __name__)

        # Check if the module defines a "blueprint" object. If not, issue a Python
        # warning.
        blueprint = getattr(module, "blueprint", None)
        if not blueprint:
            warnings.warn(
                f'{module.__name__} does not define expected "blueprint" object'
            )
            continue
        # If so, add it automatically to the list of blueprints.
        blueprints.append(blueprint)
