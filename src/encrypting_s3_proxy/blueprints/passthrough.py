"""Blueprints for passthrough logic."""
import requests
from flask import Blueprint, Response, current_app, request

blueprint = Blueprint(__name__.split(".")[-1], __name__)

_HTTP_METHODS = {
    "GET",
    "HEAD",
    "POST",
    "PUT",
    "DELETE",
    "CONNECT",
    "OPTIONS",
    "TRACE",
    "PATCH",
}

# "Hop-by-hop headers" defined by RFC 2616 section 13.5.1 ref.
# https://www.rfc-editor.org/rfc/rfc2616#section-13.5.1
_HOP_BY_HOP_HEADERS = {
    "connection",
    "keep-alive",
    "proxy-authenticate",
    "proxy-authorization",
    "te",
    "trailers",
    "transfer-encoding",
    "upgrade",
}


def _strip_disallowed(headers: dict[str, str]) -> dict[str, str]:
    return {k: v for k, v in headers.items() if k.lower() not in _HOP_BY_HOP_HEADERS}


# From https://flask.palletsprojects.com/en/2.0.x/patterns/singlepageapplications/
@blueprint.route("/", defaults={"path": ""})
@blueprint.route("/<path:path>", methods=_HTTP_METHODS)
def passthrough(path: str) -> Response:
    """
    Repeat the request to the S3 backend.

    This serves as a "bridge" for any elements of the S3 API that either do not need to
    be modified by the encrypting S3 proxy (such as listing buckets), or simply are not
    implemented.
    """
    forwarded_headers = _strip_disallowed(request.headers)
    req = requests.Request(  # ref. https://stackoverflow.com/a/36601467/248616
        method=request.method,
        url=request.url.replace(
            request.host_url,
            f"{current_app.s3_url.geturl()}/",  # type: ignore[attr-defined]
        ),
        headers=forwarded_headers,
        data=request.get_data(),
        cookies=request.cookies,
    )

    req.prepare()
    current_app.logger.debug(f"Passing through request: {req}")
    original_response = requests.Session().send(req.prepare())

    returned_headers = _strip_disallowed(original_response.raw.headers)
    response = Response(
        original_response.content, original_response.status_code, returned_headers
    )
    current_app.logger.debug(
        f"Returning response to {req.method} {req.url}: {response}"
    )

    return response
