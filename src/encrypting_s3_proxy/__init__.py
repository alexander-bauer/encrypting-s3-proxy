"""
Encrypting S3 proxy application.

This Flask application serves as an S3 endpoint, which is configured to use another S3
endpoint for actual storage. It transparently encrypts written object content, and
decrypts read object content.
"""
from .app import init_app

__all__ = [
    "init_app",
]
