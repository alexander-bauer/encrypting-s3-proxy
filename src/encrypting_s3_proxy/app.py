"""Flask application factory."""
import logging
import urllib.parse

from flask import Flask
from minio import Minio

from .blueprints import blueprints


def init_app() -> Flask:
    """Initialize the Flask application."""
    app = Flask(__name__)

    # Check if we are running in Gunicorn by seeing if its logger is initialized. If so,
    # we direct our logs there.
    if (gunicorn_logger := logging.getLogger("gunicorn.error")).hasHandlers():
        app.logger.handlers = gunicorn_logger.handlers
        app.logger.setLevel(gunicorn_logger.level)

    with app.app_context():
        # Register each of our blueprints.
        for blueprint in blueprints:
            app.register_blueprint(blueprint)

        # Load config from the environment.
        app.config.from_prefixed_env()

        if "S3_BACKEND" not in app.config:
            app.logger.critical("Configuration S3_BACKEND not set")
            # Go on to raise the KeyError intentionally.
        app.s3_url = urllib.parse.urlparse(  # type: ignore[attr-defined]
            app.config["S3_BACKEND"]
        )
        app.s3_backend = Minio(  # type: ignore[attr-defined]
            endpoint=app.s3_url.netloc,  # type: ignore[attr-defined]
            secure=app.s3_url.scheme == "https",  # type: ignore[attr-defined]
        )

        if "ENCRYPTION_ENGINE" not in app.config:
            app.logger.critical("Configuration ENCRYPTION_ENGINE not set")
            # Go on to raise the KeyError intentionally.
        app.encryption_engine = urllib.parse.urlparse(  # type: ignore[attr-defined]
            app.config["S3_BACKEND"]
        )
        app.s3_backend = Minio(  # type: ignore[attr-defined]
            endpoint=app.s3_url.netloc,  # type: ignore[attr-defined]
            secure=app.s3_url.scheme == "https",  # type: ignore[attr-defined]
        )

        return app
