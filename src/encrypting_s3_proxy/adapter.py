"""Tools and client implementations for transparently modifying S3 operations."""

import abc
import enum

import minio
import urllib3

from .crypt import CryptEngine


class AdapterCapabilities(enum.Flag):
    NONE = 0
    OBJECT_CONTENT_ENCRYPTION = enum.auto()
    OBJECT_KEY_ENCRYPTION = enum.auto()


class Adapter(minio.Minio, abc.ABC):
    """Adapaters are transparently-mutating S3 clients.

    Subclasses of ``Adapter`` may encrypt object content, object keys, or other
    information transparently to the client.
    """

    @property
    @classmethod
    def capabilities(cls) -> AdapterCapabilities:
        return AdapterCapabilities.NONE

    def __init__(self, crypt_engine: CryptEngine, **minio_options):
        self.engine = crypt_engine
        super().__init__(**minio_options)

    @abc.abstractmethod
    def put_object(
        self, bucket_name: str, object_name: str, data: bytes, length: int, **kwargs
    ) -> None:
        super().put_object(bucket_name, object_name, data, length, **kwargs)

    @abc.abstractmethod
    def get_object(
        self,
        bucket_name: str,
        object_name: str,
        offset: int = 0,
        length: int = 0,
        **kwargs
    ) -> urllib3.response.HTTPResponse:
        return super().get_object(bucket_name, object_name, data, length, **kwargs)


class ObjectContentEncryptionAdapter(Adapter):
    """Adapater for transparently encrypting only object content."""

    @property
    @classmethod
    def capabilities(cls) -> AdapterCapabilities:
        return AdapterCapabilities.OBJECT_CONTENT_ENCRYPTION

    def put_object(
        self, bucket_name: str, object_name: str, data: bytes, length: int, **kwargs
    ) -> None:
        return

    def get_object(
        self,
        bucket_name: str,
        object_name: str,
        offset: int = 0,
        length: int = 0,
        **kwargs
    ) -> urllib3.response.HTTPResponse:
        return None
