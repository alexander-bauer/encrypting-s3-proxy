FROM alpine:3

RUN apk add --update --no-cache python3 py3-pip

# Install requirements ahead of project for better layering.
COPY requirements/main.txt /opt/project/requirements.txt
RUN pip install -r /opt/project/requirements.txt

EXPOSE 8999
ENV GUNICORN_CMD_ARGS "--workers 4"

# Copy project files and install. This has to be fast, because it will have to
# run after every change.
COPY pyproject.toml /opt/project/
COPY src /opt/project/src
RUN pip install --no-deps /opt/project

ENTRYPOINT ["gunicorn", "--bind=127.0.0.1", "encrypting_s3_proxy:init_app"]
