# Spheode / Sphinx config

project = "Encrypting S3 Proxy"
author = "Alexander Bauer"
copyright = copyright_since(author, "2023")

highlight_language = "python"
mermaid_output_format = "raw"
