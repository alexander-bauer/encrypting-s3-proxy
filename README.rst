.. |Project| replace:: ``encrypting-s3-proxy``

.. _README:

=========
|Project|
=========

.. warning:: **This project is no longer developed.**

   I began writing the |Project| to address a real-world concern of mine: I
   want to use S3 as a target backend for my system backups, but I don't trust
   a remote storage provider with my data. To this end, I searched for a
   satisfactory transparent encryption layer with an S3 API, and came up short.

   But as of 2023-03-27, I came across `a feature request in rclone
   <https://github.com/rclone/rclone/issues/3382>`_ and `corresponding pull
   request <https://github.com/rclone/rclone/pull/6461>`_ that would serve the
   need in a much more general purpose way: with rclone supporting a `variety
   of storage backends <https://rclone.org/#providers>`_ and `transparent
   encryption on top of any of them <https://rclone.org/crypt/>`_, all that
   remains is for it to serve an S3-compatible interface.


------

===============
Original README
===============

.. warning::

   This project is under active development. While claims made here are
   assertive and in the present tense, they represent the intended state of the
   project.

The |Project| is a Python library and Docker container which
provides a transparently encrypting S3-compatible proxy. The goal is for S3
consumers (readers *and* writers) with no native encryption capabilities to use
untrusted S3 storage media.

An example use-case for the |Project| may be in offsite S3-based backups using
an untrusted hosting provider. Backup clients often target S3, and S3 hosting
providers typically offer at-rest and in-transit data encryption, but these are
protection against eavesdroppers or infiltrators, not against the hosting
provider itself.

To achieve data security in this case, an administrator deploys the |Project|,
and configures it to proxy to the hosting provider. Then, backup clients are
adjusted to use the deployed |Project| S3 endpoint. They write to and read from
buckets as normal, but the content of those requests is transparently encrypted
or decrypted by the |Project|.

-----------------
Design Guarantees
-----------------

The |Project| is designed to work with interchangable cryptographic details.
This means that it should support symmetric and asymmetric algorithms, and be
extended easily to support additional mechanisms. In the future, encryption of
object identifiers ("keys" in S3 parlance -- not to be confused with the
cryptographic sort) should also be possible. Therefore, the project is
structured in three layers: the client-facing S3 interface, an adapter, and the
cryptography engine.

The Client-Facing S3 Interface
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Many parts of the S3 API do not concern objects themselves, but rather buckets
or other server capabilities. Interactions with these may be passed through to
the back-end server unmodified.

API elements that *do* interact with objects, however, must be intercepted and
transformed. Without *object key* encryption in place, only operations which
concern object content are necessarily mutated. These operations require the
participation of a compatible ``Adapter``.

The Adapter
^^^^^^^^^^^

The ``Adapter`` is responsible for translating between broad "intents" (that
is, user requests to the client-facing S3 interface) and the actual object
storage provider: the back-end S3 interface. In-between, the ``Adapter`` is
responsible for doling out work to a cryptography engine instance, in order to
encrypt at least the object content. (Whether object keys or metadata are
encrypted or verified is up to the ``AdapaterCapabilities``.)

The Cryptography Engine
^^^^^^^^^^^^^^^^^^^^^^^

The details of what exactly happens to input data and metadata to make it
suitable for upstream storage or downstream consumption are beyond the scope of
the adapter layer. These details are the concern of a cryptography engine
interface, allowing them to be implemented (and tested) independently, or
extended with relative ease.

Subclasses of ``CryptEngine`` are responsible strictly for the capabilty to
encrypt or decrypt a chunk of data that fits in memory. The interface for doing
so may include directly using an ``io.IOBase``-compatible object, such as
making ``read()`` calls against a long stream of data. However, a
``CryptEngine`` is only ever responsible for handling a chunk of data (with a
maximum length configured *by* that ``CryptEngine``) at a time.
